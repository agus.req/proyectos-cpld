`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:06:08 10/31/2019 
// Design Name: 
// Module Name:    Muxes 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Muxess(
	input [2:0] Q,
	input P3,
	input P2,
	input P1,
	input fc3,
	input fc2,
	input fc1,
	output reg D2,
	output reg D1,
	output reg D0
    );

always@(*)
begin
			case(Q)
				3'b000:
					begin
						if(P3==1&&P2==0)
						begin
							D2=1;
							D1=0;
							D0=0;
						end
						else 
							begin
							if (P2==1&&P1==0)
								begin
									D2=0;
									D1=1;
									D0=1;
								end
							else
								begin
									D2=0;
									D1=0;
									D0=0;
								end
							end
					end
				3'b001:
					begin
						if(P3==1&&P1==0)
						begin
							D2=1;
							D1=0;
							D0=0;
						end
						else 
							begin
							if(P1==1&&P3==0)
								begin
									D2=1;
									D1=0;
									D0=1;
								end
							else
								begin
									D2=0;
									D1=0;
									D0=1;
								end
							end
					end
				3'b010:
					begin
						if(P2==1&&P1==0)
						begin
							D2=1;
							D1=1;
							D0=0;
						end
						else 
							begin
							if(P1==1&&P2==0)
								begin
									D2=1;
									D1=0;
									D0=1;
								end
							else
								begin
									D2=0;
									D1=1;
									D0=0;
								end
							end
					end
				3'b011:
					begin
						if(fc2==0)
						begin
							D2=0;
							D1=1;
							D0=1;
						end
						else
						begin
							D2=0;
							D1=0;
							D0=1;
						end
					end
				3'b100:
					begin
						if(fc3==0)
						begin
							D2=1;
							D1=0;
							D0=0;
						end
						else
						begin
							D2=0;
							D1=1;
							D0=0;
						end
					end
				3'b101:
					begin
						if(fc1==0)
						begin
							D2=1;
							D1=0;
							D0=1;
						end
						else
						begin
							D2=0;
							D1=0;
							D0=0;
						end
					end
				3'b110:
					begin
						if(fc2==0)
						begin
							D2=1;
							D1=1;
							D0=0;
						end
						else
						begin
							D2=0;
							D1=0;
							D0=1;
						end
					end
				3'b111:
					begin
						D2=0;
						D1=0;
						D0=0;
					end
			endcase
end

endmodule
