`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:55:19 10/30/2019 
// Design Name: 
// Module Name:    VarAVector 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module VarAVector(
	input I0,
	input I1,
	input I2,
	input I3,
	output reg [3:0] O
    );

always@(*)
	case(I3)
		0:begin
			case(I2)
				0:
					begin
					case(I1)
						0: 
							begin
							case(I0)
								0: O=4'b0000;
								1: O=4'b0001;
							endcase
							end
						1: 
							begin
							case(I0)
								0: O=4'b0010;
								1: O=4'b0011;
							endcase
							end
					endcase
					end
				1:
					begin
					case(I1)
						0: 
							begin
							case(I0)
								0: O=4'b0100;
								1: O=4'b0101;
							endcase
							end
						1: 
							begin
							case(I0)
								0: O=4'b0110;
								1: O=4'b0111;
							endcase
							end
					endcase
					end
			endcase
		end
		1:begin
			case(I2)
				0:
					begin
					case(I1)
						0: 
							begin
							case(I0)
								0: O=4'b1000;
								1: O=4'b1001;
							endcase
							end
						1: 
							begin
							case(I0)
								0: O=4'b1010;
								1: O=4'b1011;
							endcase
							end
					endcase
					end
				1:
					begin
					case(I1)
						0: 
							begin
							case(I0)
								0: O=4'b1100;
								1: O=4'b1101;
							endcase
							end
						1: 
							begin
							case(I0)
								0: O=4'b1110;
								1: O=4'b1111;
							endcase
							end
					endcase
					end
			endcase
		end
	endcase


endmodule
