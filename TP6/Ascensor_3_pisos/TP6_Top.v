`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:30:51 10/30/2019 
// Design Name: 
// Module Name:    TP6_Top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TP6_Top(
	input P1,
	input P2,
	input P3,
	input fc1,
	input fc2,
	input fc3,
	input CLK,
	input Reset,
	input Prueba2,
	output MS,
	output MB,
	output [6:0] Display,
	output T,
	output Pi1,Pi2,Pi3,
	output Pl1,Pl2,Pl3
    );
	wire [2:0] Dato;
	wire D2, D1, D0; 
	wire Q2, Q1, Q0;
	//wire [7:0] In2;
	//wire [7:0] In1;
	//wire [7:0] In0;
	//wire [3:0] DispIN;
	
	assign Dato[0]=Q0;
	assign Dato[1]=Q1;
	assign Dato[2]=Q2;
//Combinacionales
	//MUX
	//Entradas Piso1
	/*	assign In2[0]=(P3)&(~P2);
		assign In1[0]=(~P3)&(P2);
		assign In0[0]=(~P3)&(P2);
	//Entradas Piso2
		assign In2[1]=(P3)^(P1);//(P3)&(~P1)+(~P3)&(P1);//Si cambiamos el de arriba baja a 1 igual
		assign In1[1]=0;
		assign In0[1]=(~P3)+(P1);
	//Entradas Piso3
		assign In2[2]=(P2)^(P1);//(P2)&(~P1)+(~P2)&(P1);
		assign In1[2]=(P2)+(~P1);
		assign In0[2]=(~P2)&(P1);
	//Entradas Subo2
		assign In2[3]=0;
		assign In1[3]=(~fc2);
		assign In0[3]=1;
	//Entradas Subo3
		assign In2[4]=(~fc3);
		assign In1[4]=fc3;
		assign In0[4]=0;
	//Entradas Bajo1
		assign In2[5]=(~fc1);
		assign In1[5]=0;
		assign In0[5]=(~fc1);
	//Entradas Bajo2
		assign In2[6]=(~fc2);
		assign In1[6]=(~fc2);
		assign In0[6]=fc2;
	//Estado Extra
		assign In2[7]=1;
		assign In1[7]=1;
		assign In0[7]=1;*/
		

	//Entradas Display
	//	assign DispIN[3]=(Q2)+(Q1&Q0);
	//	assign DispIN[2]=(Q2&Q0)+(Q2&Q1);
	//	assign DispIN[1]=(Q2&(~Q1)&(~Q0))+((~Q2)&Q0)+((~Q2)&Q1);
	//	assign DispIN[0]=((~Q2)&(~Q0))+((Q2)&(Q0))+((Q2)&(Q1));
//MUX's
	Muxess M2(Dato,P3,P2,P1,fc3,fc2,fc1,D2,D1,D0);

//	MUX81 M2(In2[0],In2[1],In2[2],In2[3],In2[4],In2[5],In2[6],In2[7],Q2,Q1,Q0,D2);
//	MUX81 M1(In1[0],In1[1],In1[2],In1[3],In1[4],In1[5],In1[6],In1[7],Q2,Q1,Q0,D1);
//	MUX81 M0(In0[0],In0[1],In0[2],In0[3],In0[4],In0[5],In0[6],In0[7],Q2,Q1,Q0,D0);
//FLIP FLOP's
	FF FF2(D2,0,Reset,CLK,Q2);
	FF FF1(D1,0,Reset,CLK,Q1);
	FF FF0(D0,0,Reset,CLK,Q0);

//Salidas
	//4Bit a 7Seg
		
		C4511 SS(0,1,~Prueba2,Dato,Display);
		assign T=0;
	//Motor
		assign MS=(Q2&(~Q1)&(~Q0))+((~Q2)&Q1&Q0);
		assign MB=(Q2&(~Q1)&Q0)+(Q2&Q1&(~Q0));
	//Prueba
		assign Pi1=((~Q2)&(~Q1)&(~Q0));
		assign Pi2=((~Q2)&(~Q1)&(Q0));
		assign Pi3=((~Q2)&(Q1)&(~Q0));
		assign Pl1=Q2;
		assign Pl2=Q1;
		assign Pl3=Q0;
		
endmodule
