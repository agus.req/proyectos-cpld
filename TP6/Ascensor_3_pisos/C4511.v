`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:31:34 10/30/2019 
// Design Name: 
// Module Name:    C4511 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
module C4511(
//////////////////////////////////////////////////////////////////////////////////
    input LE,
    input BI,
    input LT,
	 input [2:0] Dato,
	 output reg [6:0] DISP
    );
	 
always @(*)
	if(LT==0)
		DISP=7'b0111101;
	else
		if(BI==0)
			DISP=7'b0000000;
		else
			if(LE==0)
				case(Dato)
					3'b000: DISP=7'b0110000;
					3'b001: DISP=7'b1101101;
					3'b010: DISP=7'b1111001;
					3'b011: DISP=7'b1111101;
					3'b100: DISP=7'b1111101;
					3'b101: DISP=7'b0111101;
					3'b110: DISP=7'b0111101;
					default: DISP=7'b0000000;
				endcase

endmodule