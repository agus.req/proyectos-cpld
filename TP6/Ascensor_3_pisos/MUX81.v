`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:43:24 10/29/2019 
// Design Name: 
// Module Name:    MUX81 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MUX81(
    input I0,
	 input I1,
	 input I2,
	 input I3,
	 input I4,
	 input I5,
	 input I6,
	 input I7,
    input S2,
	 input S1,
	 input S0,
	 output reg O
    );

always@(*)
	case(S2)
		0:begin
			case(S1)
				0:
					begin
					case(S0)
						0: O=I0;
						1: O=I1;
					endcase
					end
				1:
					begin
					case(S0)
						0: O=I2;
						1: O=I3;
					endcase
					end
			endcase
		end
		1:begin
			case(S1)
				0:
					begin
					case(S0)
						0: O=I4;
						1: O=I5;
					endcase
					end
				1:
					begin
					case(S0)
						0: O=I6;
						1: O=I7;
					endcase
					end
			endcase
		end
		default: O=I7;
	endcase

endmodule
