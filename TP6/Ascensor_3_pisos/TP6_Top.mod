MODEL
MODEL_VERSION "v1998.8";
DESIGN "TP6_Top";

/* port names and type */
INPUT S:PIN35 = P2;
INPUT S:PIN34 = P1;
INPUT S:PIN42 = Prueba2;
INPUT S:PIN37 = fc1;
INPUT S:PIN36 = P3;
INPUT S:PIN7 = CLK;
INPUT S:PIN40 = Reset;
INPUT S:PIN39 = fc3;
INPUT S:PIN38 = fc2;
OUTPUT S:PIN14 = Pi1;
OUTPUT S:PIN13 = Pi2;
OUTPUT S:PIN12 = Pi3;
OUTPUT S:PIN11 = Pl1;
OUTPUT S:PIN4 = Pl2;
OUTPUT S:PIN3 = Pl3;
OUTPUT S:PIN20 = Display<4>;
OUTPUT S:PIN19 = Display<5>;
OUTPUT S:PIN26 = Display<0>;
OUTPUT S:PIN24 = Display<2>;
OUTPUT S:PIN22 = Display<3>;
OUTPUT S:PIN18 = Display<6>;
OUTPUT S:PIN1 = MB;
OUTPUT S:PIN2 = MS;
OUTPUT S:PIN25 = Display<1>;
OUTPUT S:PIN29 = T;

/* timing arc definitions */
Prueba2_Display<0>_delay: DELAY Prueba2 Display<0>;
Prueba2_Display<2>_delay: DELAY Prueba2 Display<2>;
Prueba2_Display<3>_delay: DELAY Prueba2 Display<3>;
Prueba2_Display<4>_delay: DELAY Prueba2 Display<4>;
Prueba2_Display<5>_delay: DELAY Prueba2 Display<5>;
Prueba2_Display<6>_delay: DELAY Prueba2 Display<6>;
CLK_Pi1_delay: DELAY CLK Pi1;
CLK_Pi2_delay: DELAY CLK Pi2;
CLK_Pi3_delay: DELAY CLK Pi3;
CLK_Pl1_delay: DELAY CLK Pl1;
CLK_Pl2_delay: DELAY CLK Pl2;
CLK_Pl3_delay: DELAY CLK Pl3;
CLK_Display<4>_delay: DELAY CLK Display<4>;
CLK_Display<5>_delay: DELAY CLK Display<5>;
CLK_Display<0>_delay: DELAY CLK Display<0>;
CLK_Display<2>_delay: DELAY CLK Display<2>;
CLK_Display<3>_delay: DELAY CLK Display<3>;
CLK_Display<6>_delay: DELAY CLK Display<6>;
CLK_MB_delay: DELAY CLK MB;
CLK_MS_delay: DELAY CLK MS;

/* timing check arc definitions */
P1_CLK_setup: SETUP(POSEDGE) P1 CLK;
P2_CLK_setup: SETUP(POSEDGE) P2 CLK;
P3_CLK_setup: SETUP(POSEDGE) P3 CLK;
Reset_CLK_setup: SETUP(POSEDGE) Reset CLK;
fc1_CLK_setup: SETUP(POSEDGE) fc1 CLK;
fc2_CLK_setup: SETUP(POSEDGE) fc2 CLK;
fc3_CLK_setup: SETUP(POSEDGE) fc3 CLK;
P1_CLK_hold: HOLD(POSEDGE) P1 CLK;
P2_CLK_hold: HOLD(POSEDGE) P2 CLK;
P3_CLK_hold: HOLD(POSEDGE) P3 CLK;
Reset_CLK_hold: HOLD(POSEDGE) Reset CLK;
fc1_CLK_hold: HOLD(POSEDGE) fc1 CLK;
fc2_CLK_hold: HOLD(POSEDGE) fc2 CLK;
fc3_CLK_hold: HOLD(POSEDGE) fc3 CLK;

ENDMODEL
