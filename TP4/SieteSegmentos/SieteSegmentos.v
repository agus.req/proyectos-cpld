`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:08:20 09/04/2019 
// Design Name: 
// Module Name:    SieteSegmentos 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SieteSegmentos(
    input LE,
    input BI,
    input LT,
	 input [3:0] IN,
	 output reg [6:0] DISP,
	 output T
    );

assign T=0;
	 
always @(*)
	if(LT==1)
		DISP=7'b1111111;
	else
		if(BI==1)
			DISP=7'b0000000;
		else
			if(LE==0)
				case(IN)
					4'b0000: DISP=7'b1111110;
					4'b0001: DISP=7'b0110000;
					4'b0010: DISP=7'b1101101;
					4'b0011: DISP=7'b1111001;
					4'b0100: DISP=7'b0110011;
					4'b0101: DISP=7'b1011011;
					4'b0110: DISP=7'b1011111;
					4'b0111: DISP=7'b1110000;
					4'b1000: DISP=7'b1111111;
					4'b1001: DISP=7'b1110011;
					default: DISP=7'b0000000;
				endcase

endmodule
