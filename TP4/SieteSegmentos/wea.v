`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:21:36 09/04/2019
// Design Name:   SieteSegmentos
// Module Name:   C:/Users/Agus/Documents/SieteSegmentos/SieteSegmentos/wea.v
// Project Name:  SieteSegmentos
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: SieteSegmentos
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module wea;

	// Inputs
	reg LE;
	reg BI;
	reg LT;
	reg [3:0] IN;

	// Outputs
	wire [6:0] DISP;
	wire T;

	// Instantiate the Unit Under Test (UUT)
	SieteSegmentos uut (
		.LE(LE), 
		.BI(BI), 
		.LT(LT), 
		.IN(IN), 
		.DISP(DISP), 
		.T(T)
	);

	initial begin
		// Initialize Inputs
		LE = 1;
		BI = 1;
		LT = 1;
		IN = 4'b0000;

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 1;
		BI = 1;
		LT = 0;
		IN = 4'b0000;

		// Wait 100 ns for global reset to finish
		#100;
		LE = 1;
		BI = 0;
		LT = 1;
		IN = 4'b0000;

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 1;
		BI = 0;
		LT = 0;
		IN = 4'b0000;
		

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 0;
		BI = 0;
		LT = 0;
		IN = 4'b0100;

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 1;
		BI = 0;
		LT = 0;
		IN = 4'b0100;

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 1;
		BI = 0;
		LT = 0;
		IN = 4'b0010;

		// Wait 100 ns for global reset to finish
		#100;
		
		LE = 0;
		BI = 0;
		LT = 0;
		IN = 4'b1000;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

