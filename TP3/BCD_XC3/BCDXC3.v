`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:55:35 06/13/2019 
// Design Name: 
// Module Name:    BCDXC3 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BCDXC3(
    input A,
    input B,
    input C,
    input D,
    output W,
    output X,
    output Y,
    output Z
    );

assign W = ((A)|(B&(C|D)));
assign X = ((B&(~C&~D))|(~B&(C|D)));
assign Y = ((~C&~D)|(C&D));
assign Z = ~D;

endmodule
