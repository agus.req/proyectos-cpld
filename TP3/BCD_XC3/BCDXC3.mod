MODEL
MODEL_VERSION "v1998.8";
DESIGN "BCDXC3";

/* port names and type */
INPUT S:PIN37 = D;
INPUT S:PIN34 = C;
INPUT S:PIN40 = A;
INPUT S:PIN39 = B;
OUTPUT S:PIN35 = W;
OUTPUT S:PIN1 = Y;
OUTPUT S:PIN11 = X;
OUTPUT S:PIN4 = Z;

/* timing arc definitions */
D_W_delay: DELAY D W;
C_W_delay: DELAY C W;
A_W_delay: DELAY A W;
B_W_delay: DELAY B W;
C_X_delay: DELAY C X;
B_X_delay: DELAY B X;
D_X_delay: DELAY D X;
D_Y_delay: DELAY D Y;
C_Y_delay: DELAY C Y;
D_Z_delay: DELAY D Z;

/* timing check arc definitions */

ENDMODEL
