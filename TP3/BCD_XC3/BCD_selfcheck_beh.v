////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2003 Xilinx, Inc.
// All Right Reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /    Vendor: Xilinx 
// \   \   \/     Version : 10.1.03
//  \   \         Application : ISE
//  /   /         Filename : BCD_selfcheck.tfw
// /___/   /\     Timestamp : Thu Jun 13 16:05:27 2019
// \   \  /  \ 
//  \___\/\___\ 
//
//Command: 
//Design Name: BCD_selfcheck_beh
//Device: Xilinx
//
`timescale 1ns/1ps

module BCD_selfcheck_beh;
    reg A = 1'b0;
    reg B = 1'b0;
    reg C = 1'b0;
    reg D = 1'b0;
    wire W;
    wire X;
    wire Y;
    wire Z;


    BCDXC3 UUT (
        .A(A),
        .B(B),
        .C(C),
        .D(D),
        .W(W),
        .X(X),
        .Y(Y),
        .Z(Z));

    integer TX_ERROR = 0;
    
    initial begin  // Open the results file...
        #1800 // Final time:  1800 ns
        if (TX_ERROR == 0) begin
            $display("No errors or warnings.");
        end else begin
            $display("%d errors found in simulation.", TX_ERROR);
        end
        $stop;
    end

    initial begin
        // -------------  Current Time:  200ns
        #200;
        D = 1'b1;
        // -------------------------------------
        // -------------  Current Time:  250ns
        #50;
        CHECK_X(1'b1);
        CHECK_Y(1'b0);
        CHECK_Z(1'b0);
        // -------------------------------------
        // -------------  Current Time:  300ns
        #50;
        C = 1'b1;
        D = 1'b0;
        // -------------------------------------
        // -------------  Current Time:  350ns
        #50;
        CHECK_Z(1'b1);
        // -------------------------------------
        // -------------  Current Time:  400ns
        #50;
        D = 1'b1;
        // -------------------------------------
        // -------------  Current Time:  450ns
        #50;
        CHECK_Y(1'b1);
        CHECK_Z(1'b0);
        // -------------------------------------
        // -------------  Current Time:  500ns
        #50;
        B = 1'b1;
        C = 1'b0;
        D = 1'b0;
        // -------------------------------------
        // -------------  Current Time:  550ns
        #50;
        CHECK_Z(1'b1);
        // -------------------------------------
        // -------------  Current Time:  600ns
        #50;
        D = 1'b1;
        // -------------------------------------
        // -------------  Current Time:  650ns
        #50;
        CHECK_W(1'b1);
        CHECK_X(1'b0);
        CHECK_Y(1'b0);
        CHECK_Z(1'b0);
        // -------------------------------------
        // -------------  Current Time:  700ns
        #50;
        C = 1'b1;
        D = 1'b0;
        // -------------------------------------
        // -------------  Current Time:  750ns
        #50;
        CHECK_Z(1'b1);
        // -------------------------------------
        // -------------  Current Time:  800ns
        #50;
        D = 1'b1;
        // -------------------------------------
        // -------------  Current Time:  850ns
        #50;
        CHECK_Y(1'b1);
        CHECK_Z(1'b0);
        // -------------------------------------
        // -------------  Current Time:  900ns
        #50;
        A = 1'b1;
        B = 1'b0;
        C = 1'b0;
        D = 1'b0;
        // -------------------------------------
        // -------------  Current Time:  950ns
        #50;
        CHECK_Z(1'b1);
        // -------------------------------------
        // -------------  Current Time:  1000ns
        #50;
        D = 1'b1;
        // -------------------------------------
        // -------------  Current Time:  1050ns
        #50;
        CHECK_X(1'b1);
        CHECK_Y(1'b0);
        CHECK_Z(1'b0);
        // -------------------------------------
        // -------------  Current Time:  1100ns
        #50;
        A = 1'b0;
        D = 1'b0;
        // -------------------------------------
        // -------------  Current Time:  1150ns
        #50;
        CHECK_W(1'b0);
        CHECK_X(1'b0);
        CHECK_Y(1'b1);
        CHECK_Z(1'b1);
        // -------------------------------------
        // -------------  Current Time:  1800ns
        #650;
        D = 1'b1;
    end

    task CHECK_W;
        input NEXT_W;

        #0 begin
            if (NEXT_W !== W) begin
                $display("Error at time=%dns W=%b, expected=%b", $time, W, NEXT_W);
                TX_ERROR = TX_ERROR + 1;
            end
        end
    endtask
    task CHECK_X;
        input NEXT_X;

        #0 begin
            if (NEXT_X !== X) begin
                $display("Error at time=%dns X=%b, expected=%b", $time, X, NEXT_X);
                TX_ERROR = TX_ERROR + 1;
            end
        end
    endtask
    task CHECK_Y;
        input NEXT_Y;

        #0 begin
            if (NEXT_Y !== Y) begin
                $display("Error at time=%dns Y=%b, expected=%b", $time, Y, NEXT_Y);
                TX_ERROR = TX_ERROR + 1;
            end
        end
    endtask
    task CHECK_Z;
        input NEXT_Z;

        #0 begin
            if (NEXT_Z !== Z) begin
                $display("Error at time=%dns Z=%b, expected=%b", $time, Z, NEXT_Z);
                TX_ERROR = TX_ERROR + 1;
            end
        end
    endtask

endmodule

