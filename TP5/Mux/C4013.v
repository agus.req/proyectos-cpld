`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:40:11 10/02/2019 
// Design Name: 
// Module Name:    C4013 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module C4013(
	input D,
	input S,
	input R,
	input Clk,
	output reg Q
    );

always @(posedge Clk)
		Q<=D;
endmodule
