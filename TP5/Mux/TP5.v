`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:54:52 10/02/2019 
// Design Name: 
// Module Name:    TP5 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TP5(
		output [3:0] T,
		input Clk,
		output [6:0] Display
    );
	 wire U4A_Out;
	 wire U4B_Out;
	 wire U4A_Out_N;
	 wire [1:0] U2_Out;
	 wire [1:0] U3_Out;
	 wire [3:0] U1_In;
	//U4:A                 
		C4013 U4A(U4B_Out,1'b0,1'b1,Clk,U4A_Out);
	//U4:B
		assign U4A_Out_N=~U4A_Out;
		C4013 U4B(U4A_Out_N,1'b1,1'b0,Clk,U4B_Out);
	//U2
		MUX4052 U2(4'b1000,4'b0010,U4A_Out,U4B_Out,1'b0,U2_Out[0],U2_Out[1]);
	//U3
		MUX4052 U3(4'b0100,4'b0001,U4A_Out,U4B_Out,1'b0,U3_Out[0],U3_Out[1]);
	//U1
		assign U1_In[0]=U3_Out[1];
		assign U1_In[1]=U3_Out[0];
		assign U1_In[2]=U2_Out[1];
		assign U1_In[3]=U2_Out[0];
		C4511 U1(1'b0,1'b1,1'b1,U1_In,Display);
	//U5:A
		assign T[3]= ~((U4A_Out)&(~U4B_Out));
	//U5:B
		assign T[2]= ~((~U4A_Out)&(~U4B_Out));
	//U5:C
		assign T[1]= ~((~U4A_Out)&(U4B_Out));
	//U5:D
		assign T[0]= ~((U4A_Out)&(U4B_Out));

endmodule

