`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:57:14 10/02/2019 
// Design Name: 
// Module Name:    MUX4052 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MUX4052(
	 input [3:0] X,
	 input [3:0] Y,
    input A,
	 input B,
    input INH,
	 output reg X_o,
    output reg Y_o
    );

always @(*)
	if(INH==0)
		case(A)
			0:
				case(B)
					0:
					begin
						Y_o=Y[0];
						X_o=X[0];
					end
					1:
					begin
						Y_o=Y[1];
						X_o=X[1];
					end
				endcase
			1:
				case(B)
					0:
					begin
						Y_o=Y[2];
						X_o=X[2];
					end
					1:
					begin
						Y_o=Y[3];
						X_o=X[3];
					end
				endcase
		endcase
	else
	begin
		Y_o=0;
		X_o=0;
	end

endmodule
