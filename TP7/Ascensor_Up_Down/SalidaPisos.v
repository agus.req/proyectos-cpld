`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:28:04 11/09/2019 
// Design Name: 
// Module Name:    SalidaPisos 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SalidaPisos(
	input [2:0]Q,
	input EEnter,
	output reg [5:1]RespPiso
    );

always@(*)
	if(EEnter==1)
		case(Q)
			3'b000: RespPiso=5'b00001;
			3'b001: RespPiso=5'b00010;
			3'b010: RespPiso=5'b00100;
			3'b011: RespPiso=5'b01000;
			3'b100: RespPiso=5'b10000;
			default: RespPiso=5'b00000;
		endcase
endmodule
