`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:01:33 11/13/2019 
// Design Name: 
// Module Name:    AntiRebote 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AntiRebote(
	input Entrada,
	input Clock,
	output Salida
    );
	 
	 wire [1:0] Qus;
	 wire Combi;
	 
	 FF A_R4(Entrada,1'b0,~Entrada,Clock,Qus[1]);
	 FF A_ROUT(Qus[1],1'b0,~Entrada,Clock,Qus[0]);
	 
	 assign Combi=Qus[1]^Qus[0];
	 
	 assign Salida=Combi&Qus[1];


endmodule

