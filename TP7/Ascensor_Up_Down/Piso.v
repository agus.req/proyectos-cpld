`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:56:18 11/09/2019 
// Design Name: 
// Module Name:    Piso 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Piso(
	input ButtonUp,ButtonDown,ButtonEnter,
	input puReset,pCLK,
	output [5:1] pRP,
	output [6:0] DisplayPu
    );
	
	wire [2:0] puD;
	wire [2:0] puQ;
	wire Up;
	wire Down;
	wire Enter;
	
	AntiRebote Arriba(ButtonUp,pCLK,Up);
	AntiRebote Abajo(ButtonDown,pCLK,Down);
	AntiRebote Ir(ButtonEnter,pCLK,Enter);
	
	MuxPiso MPi(puQ,Up,Down,puD);
	
	FF PuQ2(puD[2],1'b0,puReset,pCLK,puQ[2]);
	FF PuQ1(puD[1],1'b0,puReset,pCLK,puQ[1]);
	FF PuQ0(puD[0],1'b0,puReset,pCLK,puQ[0]);
	
	C4511_2 DPiso(1'b0,1'b1,1'b1,puQ,DisplayPu);
	
	SalidaPisos SalP(puQ,Enter,pRP);
	


endmodule
