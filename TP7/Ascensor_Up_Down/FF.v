`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:14:30 11/02/2019 
// Design Name: 
// Module Name:    FF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FF(
    input D,
    input S,
    input R,
	 input CLK,
    output reg Q
    );

always @(posedge CLK)
	if((S==1)&&(R==0))
		Q<=1;
	else if((S==0)&&(R==1))
		Q<=0;
	else if((S==0)&&(R==0))
		Q<=D;
		
endmodule
