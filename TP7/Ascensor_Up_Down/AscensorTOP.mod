MODEL
MODEL_VERSION "v1998.8";
DESIGN "AscensorTOP";

/* port names and type */
INPUT S:PIN6 = CLK;
INPUT S:PIN35 = Reset;
INPUT S:PIN37 = fc<1>;
INPUT S:PIN39 = fc<3>;
INPUT S:PIN40 = fc<4>;
INPUT S:PIN42 = fc<5>;
INPUT S:PIN38 = fc<2>;
INPUT S:PIN27 = Abajo;
INPUT S:PIN33 = Arriba;
INPUT S:PIN34 = Ir;
OUTPUT S:PIN28 = T<1>;
OUTPUT S:PIN26 = DisplayOUT<0>;
OUTPUT S:PIN25 = DisplayOUT<1>;
OUTPUT S:PIN24 = DisplayOUT<2>;
OUTPUT S:PIN22 = DisplayOUT<3>;
OUTPUT S:PIN20 = DisplayOUT<4>;
OUTPUT S:PIN19 = DisplayOUT<5>;
OUTPUT S:PIN18 = DisplayOUT<6>;
OUTPUT S:PIN14 = MB;
OUTPUT S:PIN13 = MS;
OUTPUT S:PIN44 = T<0>;

/* timing arc definitions */
CLK_T<1>_delay: DELAY CLK T<1>;
CLK_DisplayOUT<0>_delay: DELAY CLK DisplayOUT<0>;
CLK_DisplayOUT<1>_delay: DELAY CLK DisplayOUT<1>;
CLK_DisplayOUT<2>_delay: DELAY CLK DisplayOUT<2>;
CLK_DisplayOUT<3>_delay: DELAY CLK DisplayOUT<3>;
CLK_DisplayOUT<4>_delay: DELAY CLK DisplayOUT<4>;
CLK_DisplayOUT<5>_delay: DELAY CLK DisplayOUT<5>;
CLK_DisplayOUT<6>_delay: DELAY CLK DisplayOUT<6>;
CLK_MB_delay: DELAY CLK MB;
CLK_MS_delay: DELAY CLK MS;
CLK_T<0>_delay: DELAY CLK T<0>;

/* timing check arc definitions */
Abajo_CLK_setup: SETUP(POSEDGE) Abajo CLK;
Arriba_CLK_setup: SETUP(POSEDGE) Arriba CLK;
Ir_CLK_setup: SETUP(POSEDGE) Ir CLK;
Reset_CLK_setup: SETUP(POSEDGE) Reset CLK;
fc<1>_CLK_setup: SETUP(POSEDGE) fc<1> CLK;
fc<2>_CLK_setup: SETUP(POSEDGE) fc<2> CLK;
fc<3>_CLK_setup: SETUP(POSEDGE) fc<3> CLK;
fc<4>_CLK_setup: SETUP(POSEDGE) fc<4> CLK;
fc<5>_CLK_setup: SETUP(POSEDGE) fc<5> CLK;
Abajo_CLK_hold: HOLD(POSEDGE) Abajo CLK;
Arriba_CLK_hold: HOLD(POSEDGE) Arriba CLK;
Ir_CLK_hold: HOLD(POSEDGE) Ir CLK;
Reset_CLK_hold: HOLD(POSEDGE) Reset CLK;
fc<1>_CLK_hold: HOLD(POSEDGE) fc<1> CLK;
fc<2>_CLK_hold: HOLD(POSEDGE) fc<2> CLK;
fc<3>_CLK_hold: HOLD(POSEDGE) fc<3> CLK;
fc<4>_CLK_hold: HOLD(POSEDGE) fc<4> CLK;
fc<5>_CLK_hold: HOLD(POSEDGE) fc<5> CLK;

ENDMODEL
