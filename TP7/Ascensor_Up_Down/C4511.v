`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:40:21 11/02/2019 
// Design Name: 
// Module Name:    C4511 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module C4511(
    input LE,
    input BI,
    input LT,
	 input [3:0] Dato,
	 output reg [6:0] DISP
    );
	 
always @(*)
	if(LT==0)
		DISP=7'b0111101;
	else
		if(BI==0)
			DISP=7'b0000000;
		else
			if(LE==0)
				case(Dato)
					4'b0000: DISP=7'b0110000;
					4'b0001: DISP=7'b1101101;
					4'b0010: DISP=7'b1111001;
					4'b0011: DISP=7'b0110011;
					4'b0100: DISP=7'b1011011;
					4'b0101: DISP=7'b1111101;
					4'b0110: DISP=7'b1111101;
					4'b0111: DISP=7'b1111101;
					4'b1000: DISP=7'b1111101;
					4'b1001: DISP=7'b0111101;
					4'b1010: DISP=7'b0111101;
					4'b1011: DISP=7'b0111101;
					4'b1100: DISP=7'b0111101;
					4'b1101: DISP=7'b0000000;
					4'b1110: DISP=7'b0000000;
					4'b1111: DISP=7'b0000000;
					default: DISP=7'b0000000;
				endcase

endmodule
