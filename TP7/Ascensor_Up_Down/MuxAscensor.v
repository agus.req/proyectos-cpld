`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:08:41 11/02/2019 
// Design Name: 
// Module Name:    MuxAscensor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MuxAscensor(
	input [3:0] Q,
	input [5:1] RP,
	input [5:1] fc,
	output reg [3:0] D
    );
	 
always@(*)
begin
	case(Q)
		4'b0000:
		begin
			if(RP[2]==1)
				D=4'b0101;
			else if(RP[3]==1)
				D=4'b0110;
			else if(RP[4]==1)
				D=4'b0111;
			else if(RP[5]==1)
				D=4'b1000;
			else
				D=4'b0000;
		end
		4'b0001:
		begin
			if(RP[1]==1)
				D=4'b1001;
			else if(RP[3]==1)
				D=4'b0110;
			else if(RP[4]==1)
				D=4'b0111;
			else if(RP[5]==1)
				D=4'b1000;
			else
				D=4'b0001;
		end
		4'b0010:
		begin
			if(RP[1]==1)
				D=4'b1001;
			else if(RP[2]==1)
				D=4'b1010;
			else if(RP[4]==1)
				D=4'b0111;
			else if(RP[5]==1)
				D=4'b1000;
			else
				D=4'b0010;
		end
		4'b0011:
		begin
			if(RP[1]==1)
				D=4'b1001;
			else if(RP[2]==1)
				D=4'b1010;
			else if(RP[3]==1)
				D=4'b1011;
			else if(RP[5]==1)
				D=4'b1000;
			else
				D=4'b0011;
		end
		4'b0100:
		begin
			if(RP[1]==1)
				D=4'b1001;
			else if(RP[2]==1)
				D=4'b1010;
			else if(RP[3]==1)
				D=4'b1011;
			else if(RP[4]==1)
				D=4'b1100;
			else
				D=4'b0100;
		end
		4'b0101:
		begin
			if(fc[2]==0)
				D=4'b0101;
			else
				D=4'b0001;
		end
		4'b0110:
		begin
			if(fc[3]==0)
				D=4'b0110;
			else
				D=4'b0010;
		end
		4'b0111:
		begin
			if(fc[4]==0)
				D=4'b0111;
			else
				D=4'b0011;
		end
		4'b1000:
		begin
			if(fc[5]==0)
				D=4'b1000;
			else
				D=4'b0100;
		end
		4'b1001:
		begin
			if(fc[1]==0)
				D=4'b1001;
			else
				D=4'b0000;
		end
		4'b1010:
		begin
			if(fc[2]==0)
				D=4'b1010;
			else
				D=4'b0001;
		end
		4'b1011:
		begin
			if(fc[3]==0)
				D=4'b1011;
			else
				D=4'b0010;
		end
		4'b1100:
		begin
			if(fc[4]==0)
				D=4'b1100;
			else
				D=4'b0011;
		end
		4'b1101:	D=4'b0000;
		4'b1110:	D=4'b0000;
		4'b1111:	D=4'b0000;
		default:	D=4'b0000;
	endcase
end


endmodule
