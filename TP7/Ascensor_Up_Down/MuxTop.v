`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:53:03 11/09/2019 
// Design Name: 
// Module Name:    MuxTop 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MuxTop(
	input TOPD,
	input [6:0] DDisplay1,
	input [6:0] DDisplay2,
	output reg [1:0] Tr,
	output reg [6:0] DisplayFULL
    );
	 
always@(*)
	case(TOPD)
		0:
		begin
			Tr=2'b01;
			DisplayFULL=DDisplay1;
		end
		1:
		begin
			Tr=2'b10;
			DisplayFULL=DDisplay2;
		end
	endcase

endmodule
