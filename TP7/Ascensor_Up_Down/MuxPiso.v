`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:57:18 11/09/2019 
// Design Name: 
// Module Name:    MuxPiso 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MuxPiso(
	input [2:0] Q,
	input UUp, DDown,
	output reg [2:0]D
	);

always@(*)
begin
	case(Q)
		3'b000:
			begin
				if(UUp==1&&DDown==0)
					D=3'b001;
			end
		3'b001:
			begin
				if(UUp==1&&DDown==0)
					D=3'b010;
				if(DDown==1&&UUp==0)
					D=3'b000;
			end
		3'b010:
			begin
				if(UUp==1&&DDown==0)
					D=3'b011;
				if(DDown==1&&UUp==0)
					D=3'b001;
			end
		3'b011:
			begin
				if(UUp==1&&DDown==0)
					D=3'b100;
				if(DDown==1&&UUp==0)
					D=3'b010;
			end
		3'b100:
			begin
				if(DDown==1&&UUp==0)
					D=3'b011;
			end
		default:	D=3'b000;
	endcase
end

endmodule
