`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:07:44 11/02/2019 
// Design Name: 
// Module Name:    Ascensor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Ascensor(
	input [5:1] ARP,
	input [5:1] Afc,
	input AReset,ACLK,
	output AMB,AMS,
	output [6:0] ADisplay
    );
	 wire [3:0] AD;
	 wire [3:0] AQ;
	 
	 MuxAscensor MA(AQ,ARP,Afc,AD);
	 
	 FF AQ3(AD[3],1'b0,AReset,ACLK,AQ[3]);
	 FF AQ2(AD[2],1'b0,AReset,ACLK,AQ[2]);
	 FF AQ1(AD[1],1'b0,AReset,ACLK,AQ[1]);
	 FF AQ0(AD[0],1'b0,AReset,ACLK,AQ[0]);
	 
	 C4511 D(1'b0,1'b1,1'b1,AQ,ADisplay);
	 
	 assign AMB=((AQ[3]&AQ[2])+(AQ[3]&AQ[0])+(AQ[3]&AQ[1]));
	 assign AMS=((AQ[3]&(~AQ[2])&(~AQ[1])&(~AQ[0]))+(AQ[2]&AQ[0])+(AQ[2]&AQ[1]));
	 


endmodule
