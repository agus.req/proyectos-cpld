`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:58:14 11/02/2019 
// Design Name: 
// Module Name:    AscensorTOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AscensorTOP(
	input Arriba,Abajo,Ir,			//Selector de Pisos
	input [5:1] fc,					//Sensores de Final de Carrera
	input Reset,CLK,
	output MB,MS,						//Motor Bajar y Motor Subir
	output [6:0] DisplayOUT,
	output [1:0]T
    );

	 wire [5:1] RP;					//Respuesta de puerta cerrada hacia el sistema del ascensor
	 wire [6:0] Display1;			//Display Piso
	 wire [6:0] Display2;			//Display Piso al cual moverse
	 wire TopD;
	 
	 Ascensor Asc(RP,fc,Reset,CLK,MB,MS,Display1);
	 Piso Pisos(Arriba,Abajo,Ir,Reset,CLK,RP,Display2);
	 //Puertas Puer(P5,P4,P3,P2,P1,PA,PC,SP,A,Reset,CLK,RP,MCP,MAP);
	 FF TopQ(~TopD,1'b0,Reset,CLK,TopD);
	 MuxTop MT(TopD,Display1,Display2,T,DisplayOUT);


endmodule
